<div class="show-for-large-up contain-to-grid brand-name-holder">
	<nav class="top-bar brand-name-section" data-topbar>
		<ul class="title-area">
			<!-- Title Area -->
			<?php $site_description = get_bloginfo( 'description' ); ?>
			<li class="name">
				<h1> <a class="brand-name" href="<?php echo home_url(); ?>" rel="nofollow"><?php bloginfo('name'); ?></a></h1>
			</li>
		</ul>

		<div class="site-tagline left show-for-large-up">
			<?php echo $site_description; ?>
		</div>
		
		<div class="header-contact-info right">
			<div class="row">
				<div class="large-6 medium-6 columns">
					<p>LG-41 CITYLAND MEGAPLAZA</p>
					<p>ADB Avenue, Ortigas, Pasig City</p>
				</div>
				<div class="large-6 medium-6 columns">
					<p>MON-SAT: 10:00 A.M. - 7:00 P.M.</p>
					<p><span class="icon-phone"></span> (+63) 2-9030212 / <span class="icon-mobile"></span> (+63) 917-6305418</p>
				</div>
			</div>
		</div>
	</nav>

	<nav class="top-bar top-nav" data-topbar>
		<section class="top-bar-section left">
				<?php joints_top_nav(); ?>
		</section>
	</nav>
</div>

<div class="hide-for-large-up">
	<nav class="tab-bar">
		<section class="middle tab-bar-section">
			<h1 class="title"><?php bloginfo('name'); ?></h1>
		</section>
		<section class="left-small">
			<a href="#" class="left-off-canvas-toggle menu-icon" ><span></span></a>
		</section>
	</nav>
</div>
						
<aside class="left-off-canvas-menu hide-for-large-up">
	<ul class="off-canvas-list">
		<li><label>Navigation</label></li>
			<?php joints_off_canvas(); ?>    
	</ul>
</aside>

<a class="exit-off-canvas"></a>

<br>