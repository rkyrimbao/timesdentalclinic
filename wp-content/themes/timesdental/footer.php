					<footer class="footer" role="contentinfo">

						<?php

						 	 $footer_sidebar_id = 'footer';

						 	 $footer_sidebar_array = wp_get_sidebars_widgets();

						?>

						<?php if ( count($footer_sidebar_array[$footer_sidebar_id]) > 0 && $post->post_name != 'contact-us' ) : ?>

							<div id="inner-footer" class="row">

						 		<div class="large-12 columns">

							 		<ul class="large-block-grid-<?php echo count($footer_sidebar_array[$footer_sidebar_id]); ?> small-block-grid-1">

							 			<?php dynamic_sidebar( $footer_sidebar_id ); ?>

							 		</ul>

							 	</div>

							</div> <!-- end #inner-footer -->

						<?php endif; ?>

						<div id="inner-footer" class="row <?php if ($post->post_name != 'contact-us') : ?>footer-navbar<?php endif; ?> hide-for-small-only">

							<div class="large-3 medium-3 columns">

								<p class="source-org copyright">&copy; <?php echo date('Y'); ?> <?php bloginfo('name'); ?></p>
								
		    				</div>

							<div class="large-9 medium-9 columns">

								<nav role="navigation" class="footer-nav right">

		    						<?php joints_footer_links(); ?>

		    					</nav>

							</div>

						</div> <!-- end #inner-footer -->

					</footer> <!-- end .footer -->

				</div> <!-- end #container -->

			</div> <!-- end .inner-wrap -->

		</div> <!-- end .off-canvas-wrap -->

		<?php wp_footer(); ?>

	</body>

</html> <!-- end page -->