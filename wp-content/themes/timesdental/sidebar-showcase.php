<?php if ( is_active_sidebar( 'showcase' ) ) : ?>

	<div id="showcase" class="showcase row" role="complementary">
		
		<?php dynamic_sidebar( 'showcase' ); ?>

	</div>

<?php endif; ?>