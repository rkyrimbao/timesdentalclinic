<?php get_header(); ?>
			
			<?php if ( is_front_page() ) : ?>

				<section class="times-dental-slider">

					<div class="row">

						<?php echo do_shortcode('[smartslider2 slider="1"]'); ?>

					</div>

				</section>

			<br>

			<?php endif; ?>

			<div id="content">
				
				<div id="inner-content" class="row">
			
				    <div id="main" class="large-8 medium-8 columns" role="main">
					
					    	<?php get_template_part( 'parts/loop', 'page' ); ?>
					    
					    	<?php get_sidebar('showcase'); ?>

    				</div> <!-- end #main -->
 	
				    <?php get_sidebar('sidebar'); ?>
				    
				</div> <!-- end #inner-content -->
    
			</div> <!-- end #content -->

<?php get_footer(); ?>