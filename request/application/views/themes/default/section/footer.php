 
<section class="footer-section">
	<div class="row">
	 	<div class="large-3 columns">
	 		<?php echo anchor('contact_us', '<img src="'.$assets_dir.'/img/blank.gif" class="landmark" alt="Times Dental Clinic Landmark">', array('class' => 'site-landmark')); ?>
	 		 
	 	</div>
	 	<div class="large-3 columns location">
	 		<h4><strong><u>Our Location</u></strong></h4>
	 		<h5><strong>Times Dental Clinic</strong></h5>
	 		<p>L/G Unit 41, Cityland Megaplaza</p>
	 		<p>ADB Avenue cor. Garnet Road</p>
	 		<p>Ortigas, Pasig City</p>
	 	</div>

	 		<div id="fb-root"></div>
	 		
			<script>
				(function(d, s, id) {

  					var js, fjs = d.getElementsByTagName(s)[0];

  					if (d.getElementById(id)) return;

  					js = d.createElement(s); js.id = id;

  					js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.3&appId=247753785312733";

  					fjs.parentNode.insertBefore(js, fjs);

				} (document, 'script', 'facebook-jssdk'));
			</script>

	 	<div class="large-3 columns">
	 		<div class="fb-page" data-href="https://www.facebook.com/timesdentalclinic/timeline" data-hide-cover="false" data-show-facepile="true" data-show-posts="false"><div class="fb-xfbml-parse-ignore"><blockquote cite="https://www.facebook.com/timesdentalclinic/timeline"><a href="https://www.facebook.com/timesdentalclinic/timeline">Times Dental Clinic</a></blockquote></div></div>
	 	</div>

	 	<div class="large-3 columns">
	 		<div class="icons-holder text-right">
	 			<a href="" class="social-icon icon-facebook"></a>
	 			<a href="" class="social-icon icon-twitter"></a>
	 			<a href="" class="social-icon icon-insta"></a>
	 		</div>

	 		<div class="footer-nav text-right">
	 			<?php 
	 				echo anchor(base_url(), 'Home'); 
	 				echo anchor('about', 'About Us');
	 				echo anchor('services', 'Our Services');
	 				echo anchor(base_url().'appointment', 'Appointment Request');
	 				echo anchor('faq', 'FAQ');
	 				echo anchor('contact_us', 'Contact Us');
	 			?> 
	 		</div>
	 	</div>
	</div>
</section>

 