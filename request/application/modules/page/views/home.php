<div class="row">
	<div class="large-12 columns">
		<div class="row">
			<div class="large-6 columns">
				<div class="slider-holder">
					<div class="single-item-rtl">
					    <div><img src="<?php echo $assets_dir; ?>img/blank.gif" style="background-image: url('<?php echo $assets_dir; ?>/img/office.jpg'); background-size: cover; background-repeat: no-repeat; width: 100%; height: 340px;"></div>
					    <div><img src="<?php echo $assets_dir; ?>img/blank.gif" style="background-image: url('<?php echo $assets_dir; ?>/img/model.jpg'); background-size: cover; background-repeat: no-repeat; width: 100%; height: 340px;"></div>
					    <div><img src="<?php echo $assets_dir; ?>img/blank.gif" style="background-image: url('<?php echo $assets_dir; ?>/img/model-2.jpg'); background-size: cover; background-repeat: no-repeat; width: 100%; height: 340px;"></div>
					    <div><img src="<?php echo $assets_dir; ?>img/blank.gif" style="background-image: url('<?php echo $assets_dir; ?>/img/model-3.jpg'); background-size: cover; background-repeat: no-repeat; width: 100%; height: 340px;"></div>
					 </div>
				</div>
			</div>
			<div class="large-6 columns">
				<h2 class="main-header"><strong>Welcome to TIMES DENTAL CLINIC!</strong></h2>
				<p>This website showcases the overview of our philosophy, dental services, practices, procedures, treatments, and all health essential information.</p>
				<p>TIMES DENTAL CLINIC brings the best in the field of dentistry. We have been working with our clientele to provide the finest dental services coupled with genuine concern for them. We make sure that all the staff members are fully committed to bring you comfort and satisfaction. Our goal is for you to always wear a perfect smile by giving importance to your oral healthcare.</p>

				<p><a href="<?php echo base_url(); ?>appointment" class="button right radius">Request an Appoinment</a></p>
			</div>
		</div>

		<br class="clearfix">

		<section class="article-section">
			<div class="row">
				<div class="large-5 columns">
					<h6><strong>About Us</strong></h6>
					<p>Our expertise focuses on the complete transformation of your smile. 
						Like you, your smile is unique. We take great care in designing a smile that suits you 
						perfectly. <?php echo anchor('about', 'Read more...'); ?></p>
					<p><img src="<?php echo $assets_dir; ?>/img/blank.gif" class="about-home-img"></p>
				</div>
				<div class="large-4 columns">
					<h6><strong>Why visit the Dentist?</strong></h6>
					<p>Have you ever wondered why your dentist recommends you to come back every six months? It is because regular dental visits are essential fin the maintenance of healthy teeth and gums. However, more frequent visits will be needed for high risk patients.</p>

					<blockquote>"We do have a zeal for laughter in most situations, give or take a dentist."</blockquote>			
				</div>
				<div class="large-3 columns">

					<h6><strong>Send Us A Quick Message</strong></h6>

					<?php $act = base_url().'page/quickmsg'; ?>
					<form data-abide class="quick-msg-form" method="post" name="qmsg_form" action="<?php echo $act; ?>">
						<div class="row">
							 <div class="large-12 columns<?php if (!empty(form_error('client_name'))) echo 'error'; ?>">
							 	<label>Name
							 		<?php 
							 			$name_data = array('name' => 'client_name', 'id' => 'client_name');
							 			echo form_input($name_data); 
							 		?>

							 		<?php if (!empty(form_error('client_name'))) : ?>
							 			<?php echo form_error('client_name'); ?>
							 		<?php else : ?>
							 			<small class="error">The Name field is required.</small>
							 		<?php endif; ?>

							 	</label>
							 </div>
						</div>
						<div class="row">
							 <div class="large-12 columns<?php if (!empty(form_error('client_email'))) echo 'error'; ?>">
							 	<label>E-mail
							 		<?php
							 			$email_data = array('type' => 'email', 'name' => 'client_email', 'id' => 'client_email');
							 			echo form_input($email_data);
							 		?>

							 		<?php if (!empty(form_error('client_email'))) : ?>
							 			<?php echo form_error('client_email'); ?>
							 		<?php else : ?>
							 			<small class="error">The E-mail field is required.</small>
							 		<?php endif; ?>
							 	</label>
							 </div>
						</div>
						<div class="row">
							 <div class="large-12 columns<?php if (!empty(form_error('client_phone'))) echo 'error'; ?>">
							 	<label>Phone
							 		<?php 
							 			$phone_data = array('name' => 'client_phone', 'id' => 'client_phone', 'pattern' => 'integer');
							 			echo form_input($phone_data);
							 		?>

							 		<?php if (!empty(form_error('client_phone'))) : ?>
							 			<?php echo form_error('client_phone'); ?>
							 		<?php else : ?>
							 			<small class="error">The Phone field is required.</small>
							 		<?php endif; ?>
							 	</label>
							 </div>
						</div>
						<div class="row">
							 <div class="large-12 columns<?php if (!empty(form_error('body_message'))) echo 'error'; ?>">
							 	<label>Message
							 		<?php 
							 			$msg_data = array('name' => 'body_message', 'id' => 'body_message', 'class' => 'quick-body-msg', 'cols' => '', 'rows' => '');
							 			echo form_textarea($msg_data);
							 		?>

							 		<?php if (!empty(form_error('body_message'))) : ?>
							 			<?php echo form_error('body_message'); ?>
							 		<?php else : ?>
							 			<small class="error">The message field is required.</small>
							 		<?php endif; ?>
							 	</label>
							 </div>
						</div>

						<div class="row">
							 <div class="large-12 columns">
							 	 <input type="submit" class="button radius small right" value="Submit" />
							 </div>
						</div>
					</form>
				</div>
			</div>
		</section>
	</div>
</div>