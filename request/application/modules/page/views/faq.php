<div class="row">
	<div class="large-3 columns">
		Aside
	</div>
	<div class="large-9 columns">
		<h5><strong>WHY DO I NEED X-RAYS?</strong></h5>
		<p>A dentist may take X-RAYS of the mouth, jaw or specific teeth to look for specific problems, or to check on the general health of teeth especially if you are a new patient. From this, your dentist can get an accurate picture of your dental health. The results give your dentist vital information that he/she can’t get from any other source.</p>
		<p>Children will need to have dental X-RAYS more often than adults to monitor and ensure proper growth of their adult teeth. </p>

		<h5><strong>WHAT IS A CAVITY AND HOW DOES IT FORM?</strong></h5>
		<p>CAVITY is a word that no one wants to hear at the dental clinic. A CAVITY develops when a tooth decays or breaks down. A CAVITY is a hole that can grow bigger and deeper over time. CAVITIES are also called DENTALCARIES, and it requires an immediate action to get fixed.</p>
		<p>But how does your tooth develop a hole? Blame PLAQUE. That's a sticky, slimy substance made up mostly of the germs that cause TOOTH DECAY. The bacteria in your mouth make acids and when plaque clings to your teeth, the acids can eat away at the outermost layer of the tooth, called the enamel.</p>

		<h5><strong>WHAT IS ORTHODONTIC TREATMENT AND WHY IT IS IMPORTANT?</strong></h5>
		<p>Crooked and crowded teeth are hard to clean and maintain, making them prime targets of tooth decay, gum disease and other health concerns.</p>
		<p>Thus, ORTHODONTIC TREATMENT is a series of processes to move poorly aligned teeth to a desirable position to IMPROVE THE APPEARANCE and IMPROVE THE BITING POSITION AND CHEWING FUNCTION. It also, improves self-confidence and self-image, guide facial growth for a more attractive profile and improve chances of avoiding extraction and surgical procedures.</p>

		<h5><strong>WHO NEEDS BRACES?</strong></h5>
		<p>Someone with crowded or crooked teeth needs BRACES. Warning signs include teeth that look as though they’re sitting sideways, teeth that overlap each other, and teeth that protrude significantly further than the surrounding teeth. Crowding is the most common issue addressed by BRACES.</p>

		<h5><strong>WHEN IS THE RIGHT TIME TO START BRACES?</strong></h5>
		<p>We recommend that children have an orthodontic evaluation at age 7, and adults can be treated at any age.</p>

		<h5><strong>WHAT IS DENTAL IMPLANTS AND ITS BENEFIT?</strong></h5>
		<p>DENTAL IMPLANT is the right choice for patients of all ages. It is ideal for anyone missing one, multiple, or even all of their teeth due to injury, defects, disease or decay. </p>
		<p>Dental implants provide comfort, convenience and confidence. Look younger, eat better, stay healthier and smile big. Confidence also comes from having your treatment performed by our implant specialist who has years of experience placing dental implants, building beautiful, and strong smiles.</p>

		<h5><strong>WHAT IS ROOT CANAL?</strong></h5>
		<p>A ROOT CANAL is a treatment of the pulp of the tooth that is inflamed, infected, or dead. The pulp is a soft substance in the center of the tooth that consists of nerve, blood vessels, and connective tissue. </p>
		<p>Germs causes infection in the root canal and that builds up pus at the root tip. This creates a hole in the bone. The infected tooth will never heal on it’s own, and as it gets worse, it will continue to be a source of infection that weakens your immune system. This can affect your entire body. This damage to the bone and the swelling inside the bone can also be excruciatingly painful, and even life threatening.</p>

		<h5><strong>WHY DO I NEED A CROWN?</strong></h5>
		<p>When there is a large Cavity in the tooth or previously filled large cavity starts breaking then a CROWN is needed. CROWN helps and supports in many ways.It helps protect a weak tooth from breaking or to hold together parts of a cracked tooth. It strengthens a damaged tooth by covering and protecting it.It restores a broken tooth or a tooth that has been severely worn down; holds a dental bridge in place and to cover misshaped tooth or severely discolored teeth. It alsoprovides support to damaged fragile teeth while restoring functionality and aesthetic balance. Ultimately, crowns can dramatically improve the overall appearance of your smile while making your teeth more resistant to injury.</p>

		<h5><strong>SMILE MAKE OVER</strong></h5>
		<p>Let TIMES DENTAL CLINIC handle all your dental concerns and assist you to bring out the most beautiful smile and confidence. Our dentists will first evaluate your teeth’s overall structural and aesthetic needs. Then, using a wide variety of techniques, such as porcelain veneer and crowns that reshape, change the color, and correct the alignment of the teeth, they will definitely create a smile that suits you perfectly.</p>
	</div>
</div>