<?php if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

class Appointment extends MY_Controller {

	public function __construct() {
		parent::__construct();
	}

	public function index() {
		$helper = array('form','url');
		$this->load->helper($helper);
		$this->load->library('email');
		$this->load->library('form_validation');
		$this->load->model('appointment_model');

		$this->form_validation->set_rules('fname', 'First name', 'required');
		$this->form_validation->set_rules('lname', 'Last name', 'required');
		$this->form_validation->set_rules('email', 'Email', 'required|valid_email');
		$this->form_validation->set_rules('phone', 'Phone', 'required|regex_match[/^[0-9]{7,11}$/]');

		if ($this->form_validation->run() == FALSE) {

			$this->form_validation->set_error_delimiters('<small class="error">', '</small>');

		 	$this->landingpage();

		} else {

			$data = array(
				'fname' => trim($this->input->post("fname")),
				'lname' => trim($this->input->post('lname')),
				'age' => trim($this->input->post('age')),
				'email' => trim($this->input->post('email')),
				'phone' => trim($this->input->post('phone')),
				'reason' => $this->input->post('reason'),
				'body_message' => trim($this->input->post('body_message')),
				'request_date' => trim($this->input->post('request_date'))

			);

			if ( $this->appointment_model->insert_record($data) ) {
				
				if ( $this->send_email_request($data) ) {
					// redirect('request/appointment/success_page');
					redirect('timesdentalclinic/request/appointment/success_page');
						//$this->success_page();
				}

			} else {

			}
		}
		
	}

	public function landingpage() {
		$this->_set_title('Times Dental Clinic: Request an Appointment');

		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);
		$this->_set_layout('default');
	
        	$this->_render_page('request-an-appointment', $this->data);

	}

	public function send_email_request($data) {
		$email_data['data'] = $data;
		$email_setting  = array(
			'mailtype' => 'html',
			'charset' => 'utf-8',
			'wordwrap' => TRUE
			);
		$this->email->initialize($email_setting);
		$mailTo = 'customercare@timesdentalclinic.ph';
		$mailFrom = $data['email'];

		$this->load->library('email');
		
		//$this->email->clear();

		//$this->email->from($data['email']);
		$this->email->from('site@timesdentalclinic.ph');
		$this->email->to('customercare@timesdentalclinic.ph'); 
		$this->email->reply_to($data['email']);
		$this->email->subject('Request Appointment from '. $data['fname'].' '.$data['lname']);

		$email_template = $this->load->view('email_template', $email_data, TRUE);
		$this->email->message($email_template);	
			
		$send = $this->email->send();
		
		return ($send ? TRUE : FALSE);
	}

	public function success_page() {
		$this->_set_title('Times Dental Clinic: Request an Appointment');

		$this->_set_section('offcanvas', 'section/offcanvas', $this->data);

		$this->_set_layout('simple');

        	$this->_render_page('success', $this->data);

	} 
}

/* End of file example.php */
/* Location: ./application/controllers/example.php */