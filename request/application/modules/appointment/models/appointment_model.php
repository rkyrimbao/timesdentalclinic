<?php if (!defined('BASEPATH')) exit('No direct script access allowed...');

class appointment_model extends CI_Model {
   
    function __construct() {  

        parent::__construct();  

    } 

    function insert_record ($data) {

    	$this->load->database();

    	return $this->db->insert('wp_appointment_request', $data);

    }

}