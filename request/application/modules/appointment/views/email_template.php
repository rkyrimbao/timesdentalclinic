<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Times Dental Clinic: Request an Appointment</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0"/>
	</head>
	<body>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="background-color: #32cd32; padding-top: 20px; padding-bottom: 20px; padding-left: 20px; padding-right: 20px;">
			<tr>
				<td style="color: #fff; font-size: 18px;" valign="top"><img src="http://timesdentalclinic.ph/wp-content/themes/timesdental/assets/images/timesdental-logo.png" width="300" height="60" style="vertical-align: middle;">&nbsp;<i>Timeless Smile, Timely Care</i></td>
			</tr>
			<tr>
				<td style="border-radius: 5px; background-color: #fff; color: #333; padding: 10px;  font-size: 13px;">
						<table border="0" width="80%" style="width: 80%;">
							<tr style="margin-bottom: 10px;">
								<td width="15%" valign="top"><strong>Name:</strong> </td><td><?php echo ucfirst($data['fname'].' '.$data['lname']); ?></td>
							</tr>
							<tr>
								<td width="15%" valign="top"><strong>Age:</strong> </td><td><?php echo $data['age']; ?></td>
							</tr>
							<tr>
								<td width="15%" valign="top"><strong>Contact Number:</strong> </td><td><?php echo $data['phone']; ?></td>
							</tr>
							<tr>
								<td width="15%" valign="top"><strong>Appointment Date:</strong> </td><td><?php echo $data['request_date']; ?></td>
							</tr>
							<tr>
								<td width="15%" valign="top"><strong>Category:</strong> </td><td><?php echo $data['reason']; ?></td>
							</tr>
							<tr>
								<td width="15%" valign="top"><strong>Message:</strong> </td><td><?php echo nl2br($data['body_message']); ?></td>
							</tr>
						</table>
				</td>
			</tr>
		</table>
	</body>
</html>