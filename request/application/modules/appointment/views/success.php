<script type="text/javascript">

//Datepicker Popups calender to Choose date
</script>

<div class="row show-for-medium-up">
	<div class="large-4 columns"><h1 class="site-title"><?php echo anchor(base_url(), '<img src="'.$assets_dir.'/img/blank.gif" class="site-brand">', array('class' => 'site-logo')); ?></h1></div>
	<div class="large-8 columns"><span class="site-tagline">Timeless Smile, Timely Care</span></div>
</div>

<br class="show-for-small-only">

<div class="row appointment-section">
	<div class="large-12 columns text-center">

		 	<h4>Appointment has been set.</h4>

		 	<p><?php echo anchor('http://timesdentalclinic.ph/', 'Back to homepage ', array('class' => 'button radius small text-center')); ?></p>
	
	</div>
</div>