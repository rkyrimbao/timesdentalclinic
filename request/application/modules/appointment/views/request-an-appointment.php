<script type="text/javascript">

//Datepicker Popups calender to Choose date
</script>

<div class="row show-for-medium-up">
	<div class="large-4 columns"><h1 class="site-title"><?php echo anchor(base_url(), '<img src="'.$assets_dir.'/img/blank.gif" class="site-brand">', array('class' => 'site-logo')); ?></h1></div>
	<div class="large-8 columns"><span class="site-tagline">Timeless Smile, Timely Care</span></div>
</div>

<br class="show-for-small-only">

<div class="row appointment-section">
	<div class="large-4 columns">
		<div class="row">
			<div class="large-12 columns">
				<div class="doc-img-holder">
					<img src="<?php echo $assets_dir; ?>img/blank.gif" class="doc-img">
					<center>Dr. Jayhardt Tantiado</center>
				</div>
			</div>

			<div class="large-12 columns">
				<h3>Clinic Location</h3>
				<p>L/G Unit 41, Cityland Megaplaza,</p>
				<p>ADB Avenue cor. Garnet Road,</p>
				<p>Ortigas, Pasig City , Philippines 1605</p>
			</div>

			<div class="large-12 columns">
				<h3>Clinic Schedule</h3>
				<p>Monday-Saturday: 10:00 am - 7:00 pm</p>
				<p>Sunday: by Appointment</p>
			</div>

			<div class="large-12 columns">
				<h3>For more information, Please call us at</h3>
				<p>Direct: (+632) 903-0212</p>
				<p>(+63) 917-6305418</p>
			</div>

 		</div>
	</div>
	<div class="large-8 columns" role="main">
		<h4><strong>Request an Appointment</strong></h4>
		<p>The first step toward achieving a beautiful, healthy smile is to schedule an appointment. To schedule an appointment, please complete and submit the request form below. Our scheduling coordinator will contact you soon to confirm your appointment.</p>
		<p>Please note this form is for requesting an appointment. If you need to cancel or reschedule an existing appointment, or if you require immediate attention, please contact our practice directly.</p>

		<h5><strong>Contact Information</strong></h5>

		<?php echo form_open(base_url().'appointment', array('class' => 'request-form', 'data-abide' => '')); ?>

			<div class="row">
				<div class="large-6 columns <?php #if (!empty(form_error('fname'))) echo 'error'; ?>">
					<label>First name
						<input type="text" name="fname" placeholder="First name" value="<?php echo set_value('fname'); ?>" required>
				 		<small class="error">First name is required.</small>
						</label>
					</div>
					<div class="large-6 columns  <?php #if (!empty(form_error('lname'))) echo 'error'; ?>">
						<label>Last name
							<input type="text" name="lname" placeholder="last name" value="<?php echo set_value('lname'); ?>" required>
							<small class="error">Last name is required.</small>
						</label>
					</div>
				</div>

				<div class="row">
					<div class="large-12 columns <?php #if (!empty(form_error('email'))) echo 'error'; ?>">
						<label>Contact number
							<input type="text" name="phone" placeholder="phone" pattern="integer" value="<?php echo set_value('phone'); ?>" required>
							
							<?php #if (!empty(form_error('phone'))) : ?>
					 			<?php # echo form_error('phone'); ?>
					 		<?php #else: ?>
					 			<!--<small class="error">Phone is required.</small>-->
					 		<?php #endif; ?>

						</label>
					</div>
				</div>
				<div class="row">
					<div class="large-12 columns <?php #if (!empty(form_error('email'))) echo 'error'; ?>">
						<label>E-mail address
							<input type="email" name="email" placeholder="E-mail" value="<?php echo set_value('email'); ?>" required>
							<small class="error">E-mail address is required.</small>
						</label>
					</div>
				</div>

				<div class="row">
					<div class="large-4 columns <?php #if (!empty(form_error('age'))) echo 'error'; ?>">
						<label>Age
							<input type="text" name="age" placeholder="Age" pattern="integer" value="<?php echo set_value('age'); ?>" required>
							<small class="error">Age is required.</small>
						</label>
					</div>
				</div>

				<h5><strong>Date of Schedule</strong></h5>

				<div class="row">
					<div class="large-8 columns <?php #if (!empty(form_error('request_date'))) echo 'error'; ?>">
					 	<input type="text" name="request_date" id="datepicker" placeholder="Date" value="<?php echo set_value('request_date'); ?>" required>
					 	<small class="error">Date of Schedule is required.</small>
					</div>
				</div>

				<div class="row">
					<div class="large-12 columns">
						<label>Reason for visit
							<select name="reason" id="reason" class="category-list">
								<option>Select Category</option>
								<optgroup label="Cosmetic Dentistry">
									<option value="E-max Crown">E-max Crown</option>
									<option value="Zirconia Crown">Zirconia Crown</option>
									<option value="Porcelain Veneers">Porcelain Veneers</option>
									<option value="Home Whitening Kits (Teeth Whitening)">Home Whitening Kits (Teeth Whitening)</option>
									<option value="Laser Teeth Whitening">Laser Teeth Whitening</option>
									<option value="Composite Laminate">Composite Laminate (Cosmetic Restorations)</option>
								</optgroup>
								<optgroup label="Dental Implants">
									<option value="Immediate Implant Placement">Immediate Implant Placement</option>
									<option value="Restoration of Implants">Restoration of Implants</option>
									<option value="Conventional Implant">Conventional Implant</option>
								</optgroup>
								<optgroup label="General Dentistry">
									<option value="Dental Sealant">Dental Sealant</option>
									<option value="Fluoride Therapy">Fluoride Therapy</option>
									<option value="Non-Surgical Extraction">Non-Surgical Extraction (Extractions)</option>
									<option value="Wisdom Tooth Extraction">Wisdom Tooth Extraction (Extractions)</option>
									<option value="Oral Prophylaxis">Oral Prophylaxis (Cleaning)</option>
									<option value="Root Canals Therapy">Root Canals Therapy (RCT)</option>
									<option value="Gingivitis Treatment">Gingivitis Treatment</option>
									<option value="Scaling and Root Planing">Scaling and Root Planing</option>
									<option value="Deep Scaling">Deep Scaling</option>
									<option value="Ultrasonic Scaling">Ultrasonic Scaling</option>
									<option value="Light Cured Restoration">Light Cured Restoration</option>
									<option value="Temporary Filling">Temporary Filling</option>
									<option value="Teeth Contouring and Reshaping">Teeth Contouring and Reshaping</option>
								</optgroup>
								<optgroup label="Orthodontics">
									<option value="Conventional Braces">Conventional Braces</option>
									<option value="Ceramic Braces">Ceramic Braces</option>
									<option value="Self-Ligating Braces">Self-Ligating Braces</option>
								</optgroup>
								<optgroup label="Prosthodontics">
									<option value="Porcelain Crowns and Bridges">Porcelain Crowns and Bridges</option>
									<option value="Temporary Crown">Temporary Crown</option>
									<option value="Removable Partial Dentures (Flexible)">Removable Partial Dentures (Flexible)</option>
									<option value="Removable Partial Dentures (Valplast)">Removable Partial Dentures (Valplast)</option>
									<option value="Complete Dentures">Complete Dentures</option>
									<option value="entures Adjustment">Dentures Adjustment</option>
									<option value="Dentures Repair">Dentures Repair</option>
								</optgroup>
							</select>
						</label>
					</div>
				</div>

				<h5><strong>Note to doctor...</strong></h5>

				<div class="row">
					<div class="large-12 columns <?php #if (!empty(form_error('body_message'))) echo 'error'; ?>">
						<textarea rows="10" cols="40" name="body_message" placeholder="Message"><?php echo set_value('body_message'); ?></textarea>
						<small class="error">Message is required.</small>
						<br>
						<input type="submit" name="submit" class="button small radius right" value="Submit">
					</div>
				</div>
				<?php echo form_close(); ?>
			</div>
		</div>
	</div>